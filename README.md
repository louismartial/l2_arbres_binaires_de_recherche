# TP – Arbres binaires de recherche

Le but de ce TP est d’implanter les algorithmes de recherche et de modification dans les ABR vus en cours.

Il y a quatre (couples de) fichiers :

- *ArbreBinaire.cpp/.h* contient l’implantation d’un arbre binaire (classe *ArbreBinaire*) ;
- *ABR.cpp/.h* contient l’implantation d’un arbre binaire de recherche (classe *ABR*) ;
- *tests.cpp* est un fichier de tests, qui contient le *main* ;
- *Makefile* s’occupe de la compilation : *make* compilera ce qu’il faut !

La commande *make* produit l’exécutable *tests* que vous devez exécuter pour tester vos fonctions.

Le seul fichier à compléter, et à rendre, est *ABR.cpp*.

Pour tout l’exercice, on peut tester les fonctions en vérifiant le résultat graphiquement sur le fichier généré *arbre.svg*.

## *noeud\* ABR::inserer(int k)*

Insère la valeur *k* dans l’arbre et renvoie un pointeur vers le nouveau nœud.

Exemples de test : insérer des nœuds dans un arbre initialement vide ; par exemple, insérer des nœuds de valeurs 1, 2, . . ., 15 (dans cet ordre) ; les insérer dans l’ordre inverse ; les insérer dans l’ordre 8, 7, . . ., 1, puis 9, 10, . . .,15 ; trouver un ordre d’insertion qui construit un arbre de hauteur 3. En choisissant l’arbre test (option 1), le résultat attendu est l’arbre de l'image 1.

![HTML_LOGO](/img/img_1.png)

## *void ABR::parcoursInfixe(noeud\* x)* et *noeud\* ABR::minimum(noeud* x)*

La première fonction affiche un parcours infixe de l’ABR et renvoie le nœud de valeur minimale dans cet arbre. La seconde fonction renvoie le nœud de valeur minimale dans cet arbre.

## *noeud\* ABR::rechercher(int k)*

Recherche un nœud de valeur *k* dans l’arbre. Si un tel nœud existe la fonction renvoie un pointeur sur ce nœud, sinon le pointeur *NULL*.

## *noeud\* ABR::successeur(noeud \*x)*

Renvoie le nœud successeur du nœud *x* passé en paramètre. Si *x* est le nœud de valeur maximale dans l’arbre, on renvoie le pointeur *NULL*.

## *void ABR::supprimer(noeud\* z)*

Supprime le nœud *z*. Penser à libérer la mémoire correspondante lors de la suppression d’un nœud ! Utiliser la méthode *remplacer* de *ArbreBinaire*, qui implante la fonction vue en cours.

En supprimant successivement les nœuds de valeur 23, 16, 6 et 13 de l’arbre de test, le résultat doit être celui de l'image 2.

![HTML_LOGO](/img/img_2.png)

## *void ABR::rotationGauche(noeud\* z)* et *void ABR::rotationDroite(noeud\* z)*

La première fonction effectue une rotation gauche sur le nœud *z*. La seconde fonction effectue une rotation droite sur le nœud *z*.

Après une rotation gauche sur le nœud 6 de l’arbre test, le résultat doit être celui de l'image 3.

![HTML_LOGO](/img/img_3.png)