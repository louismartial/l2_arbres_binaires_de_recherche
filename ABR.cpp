#include <cstdlib>
#include <iostream>
#include <unistd.h>
#include <string>
#include "ABR.h"

using namespace std;

//------------------------------------------------------
// Inserer un noeud z dans l'arbre A
//------------------------------------------------------

noeud* ABR::inserer(int k)
{
  noeud *z = new noeud(k);
  noeud *x = racine;
  noeud *p = NULL;
  while (x) {
    p = x;
    if (k < x->val)
      x = x->filsG;
    else
      x = x->filsD;
  }
  z->pere = p;
  if (!p)
    racine = z;
  else if (k < p->val)
    p->filsG = z;
  else
    p->filsD = z;
  return z;
}

//------------------------------------------------------
// Parcours infixe
//------------------------------------------------------

void ABR::parcoursInfixe(noeud* x)
{
  if (x) {
    parcoursInfixe(x->filsG);
    cout << x->val << " ";
    parcoursInfixe(x->filsD);
  }
}

//------------------------------------------------------
// Noeud de valeur minimale dans l'arbre
//------------------------------------------------------

noeud* ABR::minimum(noeud* x)
{
  if (x) {
    while (x->filsG)
      x = x->filsG;
  }
  return x;
}

//------------------------------------------------------
// Recherche d'un noeud de valeur k
//------------------------------------------------------

noeud* ABR::rechercher(int k)
{
  noeud* x = racine;
  while (x && x->val != k) {
    if (k < x->val)
      x = x->filsG;
    else
      x = x->filsD;
  }
  return x;
}

//------------------------------------------------------
// Recherche du successeur du noeud x
//------------------------------------------------------

noeud* ABR::successeur(noeud *x)
{
  if (x->filsD)
    return minimum(x->filsD);
  noeud *p = x->pere;
  while (p && x == p->filsD) {
    x = p;
    p = x->pere;
  }
  return p;
}

//------------------------------------------------------
// Remplacement d'un noeud x par un noeud z dans un arbre A
//------------------------------------------------------

void remplace(ABR* A, noeud* x, noeud* z) {
  noeud *p = x->pere;
  x->pere = NULL;
  if (!p)
    A->racine = z;
  
  else if (x == p->filsG)
    p->filsG = z;
  else
    p->filsD = z;
  if (z)
    z->pere = p;
}

//------------------------------------------------------
// Suppression d'un noeud
//------------------------------------------------------

void ABR::supprimer(noeud* z)
{
  if (!z->filsG)
    remplace(this, z, z->filsD);
  else if (!z->filsD)
    remplace(this, z, z->filsG);
  else {
    noeud *y = successeur(z);
    remplace(this, y, y->filsD);
    y->filsD = z->filsD;
    z->filsD = NULL;
    y->filsG = z->filsG;
    z->filsG = NULL;
    if (y->filsD)
      y->filsD->pere = y;
    if (y->filsG)
      y->filsG->pere = y;
    remplace(this, z, y);
  }  
}


//------------------------------------------------------
// Rotations
//------------------------------------------------------

void ABR::rotationGauche(noeud* z)
{
  noeud* f = z->filsD;
  noeud* p = z->pere;
  if (p) {
    if (p->filsG = z)
      p->filsG = f;
    else
      p->filsD = f;
  }
  f->pere = p;
  z->pere = f;
  z->filsD = f->filsG;
  f->filsG = z;
}

void ABR::rotationDroite(noeud* z)
{
  noeud* p = z->pere;
  noeud* f = z->filsG;
  if (p) {
    if (p->filsG == z)
      p->filsG = f;
    else
      p->filsD = f;
  }
  f->pere = p;
  z->pere = f;
  z->filsG = f->filsD;
  f->filsD = z;
}
